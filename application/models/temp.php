<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Model for interacting with database.
 *
 * @author Akash Paul
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

//model for grabbing data from db re users. 
class temp extends CI_Model {

//put your code here
    //model for obtaining info re users or old users. 
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        error_reporting(-1);
    }

    public function SelectAll() {
        $query = $this->db->query("SELECT * FROM T_USERS T");
        return $query->result();
    }

    //purposes of checking db connection if failed. 
    public function ConnectionCheck() {
        try {
            $this->load->database();
        } catch (PDOException $ex) {
            echo '' . $ex->getMessage();
        }
    }

    public function Validate() {
        $sql = "SELECT * FROM T_USERS T WHERE T.FIRSTNAME = ? AND T.SECONDNAME = ? AND T.EMAIL = ? AND T.USERNAME = ? AND T.PASSWORD = ?";
        $sqlCount = "SELECT COUNT(*) FROM T_USERS T WHERE T.FIRSTNAME = ? AND T.SECONDNAME = ? AND T.EMAIL = ? AND T.USERNAME = ? AND T.PASSWORD = ?";
        $CountExecute = $this->db->query($sqlCount, array());
        $sql = "SELECT * FROM T_USERS T WHERE T.USERNAME = '.$this->->db->escape($username).' AND '.$this->db->escape($email).'";
        $execute = $this->db->query($sql, array($firstname, $lastname, $email, $username, $password));
        if ($execute->num_rows() == 1 && $CountExecute->num_rows() == 1) {
            //execute for exsiting users.
        }
        return $this->db->query($sql, array($firstname, $lastname, $email, $username, $password));
    }

    //method for users already registered
    public function ExistingUsers() {
        $username = $this->db->escape($_POST['UsernameLogIn']);
        $password = $this->db->escape($POST['PasswordLogIn']);
        if (isset($$_POST['UsernameLogIn']) && isset($_POST['PasswordLogIn']) && !empty($username) && !empty($password)) {
            $sql = "SELECT * FROM t_registered_users T WHERE T.username = '$username' AND T.PASSWORD = '$password' LIMIT 1";
            $execute = $this->db->query($sql);
            if ($execute->num_rows() == 1) {
                $sql = "SELECT T.USERNAME,T.PASSWORD,T.SESSION_ID FROM TEST.t_registered_users T INNER JOIN TEST.t_session_activity A ON A.SESSION_ID = T.SESSION_ID WHERE T.USERNAME= ? AND T.PASSWORD = ?";
                $join = $this->db->query($sql, array($username, $password)); //take sql and process in controller...
                return $join;
            }
        }
    }

    public function GetLoginFailedAttempts() {//cross reference from login_attempts table and users for username and perhaps email as one email allowed per user ie @aston.ac.uk.
        $sql = "SELECT T.USERNAME,T.EMAIL,T.ATTEMPTS FROM TEST.t_login_attempts T 
                 INNER JOIN TEST.t_registered_users A ON A.USERNAME = T.USERNAME
                  INNER JOIN TEST.t_users B ON B.USERNAME = A.USERNAME";
    }

    public function NewUsers() {
        $Connection = $this->ConnectionCheck();
        $rowCount = "SELECT COUNT(*) FROM T_USERS T WHERE T.FIRSTNAME = ? AND T.SECONDNAME = ? AND T.EMAIL = ? AND T.USERNAME = ?";
        return $rowCount;
    }

    public function NewUserInsert() { //make emails unique only 1 allowed for a user sql - ALTER TABLE TEST.t_users ADD UNIQUE email 
        $first = $this->db->escape($_POST['firstname']);
        $second = $this->db->escape($_POST['Secondname']);
        $email = $this->db->escape($_POST['email']);
        $username = $this->db->escape($_POST['username']);
        $password = $this->db->escape(md5($_POST['password']));
        if (isset($_POST['firstname']) && isset($_POST['Secondname']) && isset($_POST['email']) && isset($_POST['username']) && $_POST['password'] && !empty($first) && !empty($second) && !empty($email) && !empty($username) && !empty($password)) {
            $sql = "SELECT * FROM T_USERS T WHERE T.FIRSTNAME = ? AND T.SECONDNAME = ? AND T.EMAIL = ? AND T.USERNAME = ? LIMIT 1";
            $execute = $this->db->query($sql, array($first, $second, $email, $username));
            if ($execute->num_rows() == 0) {
                $sql = "INSERT INTO T_USERS(FIRSTNAME,SECONDNAME,EMAIL,USERNAME,PASSWORD,SESSION_ID) VALUES($first,$second,$email,$username,$password,'null')"; //solution for binding parameters to insert statement and construct digest http authentication. call method on controller and start session and place username and user preferences in a cookie. 
                $exe = $this->db->query($sql);
                return $exe;
            }
        }
    }
    public function getUsername() {
        $this->db->start_cache();
        $this->db->select('USERNAME');
        $this->db->stop_cache();
        $this->db->get('T_USERS');
    }

    public function DatabaseConnectionClose() {
        $close = $this->db->close();
        return $close;
    }

}
